"""This is responsible for preprocessing questions and qeds if required, after preprocessing split the data in training testing
and validation data
"""
import argparse
import os
import warnings
import pandas as pd
import numpy as np
import json  
import os
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

    
column_list = ["processed_question", "qeds", "SubjectName", "questionExternalId", "stream"]


    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train-test-split-ratio", type=float, default=0.1)
    parser.add_argument("--train-val-split-ratio", type=float, default=0.1)
    parser.add_argument("--stream", type=str, default = "Foundation")
    args, _ = parser.parse_known_args()
    print("Received arguments {}".format(args))
    
    # Get arguments
    train_test_ratio = args.train_test_split_ratio
    train_val_ratio = args.train_val_split_ratio
    stream = args.stream
    print("THIS IS A PROCESSEING JOB FOR: ", stream)
    
    """Reading data from /opt/ml/preprocessing/input/completeData.csv
    
    Do Following preprocessing steps : 
    - Get tagged_data 
    - Filter df on foundations data
    - Do label encoding
    - Create train/test/val datasets
    
    """
    input_data_path = os.path.join("/opt/ml/processing/input", "tagged.csv")
    print("Reading input data from {}".format(input_data_path))
    df = pd.read_csv(input_data_path)
    df = pd.DataFrame(df)
    df = df[df.nodeShortCode != "TE12MTH19_13_01"]
    df_stream = df[df.stream==stream]
    
    # Filter
    # Drop duplicates
    print("Number of question before dropping duplicates", df_stream.shape[0])
    df_stream = df_stream.drop_duplicates(subset="question")
    print("Number of question After dropping duplicates", df_stream.shape[0])
    
    # Get rid of "This Ques cannot be processed" in processed_question column
    df_stream = df_stream[df_stream.processed_question!="This Ques cannot be processed"]
    df_stream = df_stream[column_list]
    print(df_stream.SubjectName.value_counts())
    
    # Add label encoder
    encoder = LabelEncoder()
    encoded = encoder.fit_transform(df_stream["SubjectName"])
    df_stream["target"] = encoded
    encoded_dict = dict(zip(list(encoded), list(encoder.inverse_transform(encoded))))  # to be returned
    print(encoded_dict)
    
    # Split the data into training and testing
    print("Splitting data into train and test sets with ratio {}".format(train_test_ratio))
    train_df, test_df = train_test_split(df_stream, test_size=0.1, stratify=df_stream["target"], random_state=42, shuffle=True)
    print("Splitting data into train and test sets with ratio {}".format(train_val_ratio))
    train_df, val_df = train_test_split(train_df, test_size=0.1, stratify=train_df["target"], random_state=42, shuffle=True)
    
    # Printing necessary information
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")
    print(f"Number of validation questions {val_df.shape[0]}")
    print("###### Remove Nans and reset index ########")
    train_df = train_df.dropna()
    test_df = test_df.dropna()
    val_df = val_df.dropna()
    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)
    val_df = val_df.reset_index(drop=True)
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")
    print(f"Number of validation questions {val_df.shape[0]}")

    train_features_output_path = os.path.join("/opt/ml/processing/train", "train.csv")
    test_features_output_path = os.path.join("/opt/ml/processing/test", "test.csv")
    val_features_output_path = os.path.join("/opt/ml/processing/val", "val.csv")
    
    # Saving features 
    print("Saving training features to {}".format(train_features_output_path))
    train_df.to_csv(train_features_output_path, index=False)

    print("Saving test features to {}".format(test_features_output_path))
    test_df.to_csv(test_features_output_path, index=False)

    print("Saving val features to {}".format(val_features_output_path))
    val_df.to_csv(val_features_output_path, index=False)