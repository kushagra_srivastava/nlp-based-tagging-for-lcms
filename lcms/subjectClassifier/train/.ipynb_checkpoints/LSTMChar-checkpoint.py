"""Subject Classification


This model is an LSTM based model with two inputs i.e processed_questions and qeds, 
- We will be using a biredictional LSTM to process processed_questions 
- Build a character level model to process qeds

This is a generic script for subject classification depending upon the stream passed as an input
The training, testing and validation S3_uri's is the only that changes and along with that the number of classes as well 

So the most important argument is train/test/val S3 path's and n_classes
"""

import argparse, os
import tensorflow as tf
import pandas as pd
from tensorflow.keras.layers import TextVectorization
import string
import numpy as np

############# HELPER FUNCTIONS ##########################
def max_question_length(train_df):
    train_df = train_df.reset_index(drop=True)
    max_len = 0
    for idx in train_df.index:
        pro_ques = train_df.iloc[idx]["processed_question"]
        max_len = max(max_len, len(pro_ques.split(" ")))
    return max_len

def tfds_2_inputs(df):
    X_ds = tf.data.Dataset.from_tensor_slices((df["processed_question"].values, df["qeds"].values))
    y_ds = tf.data.Dataset.from_tensor_slices(df["target"].values)
    Xy_ds = tf.data.Dataset.zip((X_ds, y_ds)).batch(32).prefetch(tf.data.AUTOTUNE)
    return Xy_ds
#########################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=1)
    parser.add_argument('--lstm_unit', type=int, default=32)
    parser.add_argument('--output_dim', type=int, default = 256)
    parser.add_argument('--n_classes', type=int, default = 6)
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--training', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])
    parser.add_argument('--testing', type=str, default=os.environ['SM_CHANNEL_TESTING'])
    
    args, _ = parser.parse_known_args()
    
    ####### ARGUMENTS ###################
    epochs     = args.epochs
    n_classes = args.n_classes
    lstm_unit     = args.lstm_unit
    output_dim = args.output_dim
    model_dir  = args.model_dir
    training_dir   = args.training
    validation_dir = args.validation
    testing_dir = args.testing
    print("LSTM UNITS USED", lstm_unit)
    print("OUTPUT DIMS USED", output_dim)
    print("Number of classes", n_classes)
    print("model dir", model_dir)
    print("training dir", training_dir)
    print("testing dir", testing_dir)
    print("validation dir", validation_dir)
    #####################################
    
    print("Reading Files")
    train_df = pd.read_csv(f"{training_dir}/train.csv")
    test_df = pd.read_csv(f"{testing_dir}/test.csv")
    val_df = pd.read_csv(f"{validation_dir}/val.csv")
    print("Done Reading Files")
    
    # Taking care of Biology
    stream_name = train_df.stream.unique()[0]
    if stream_name == "Medical":
        print("Since the stream is Medical taking care of 3 Biology question that are present") 
        train_df = train_df[train_df.SubjectName!="Biology"]
        test_df = test_df[test_df.SubjectName!="Biology"]
        val_df = val_df[val_df.SubjectName!="Biology"]
        train_df["target"] = train_df["target"].map(lambda x : x-1)
        test_df["target"] = test_df["target"].map(lambda x : x-1)
        val_df["target"] = val_df["target"].map(lambda x : x-1)
        print(dict(zip(train_df.target.unique(), train_df.SubjectName.unique())))
        print(dict(zip(val_df.target.unique(), val_df.SubjectName.unique())))
        print(dict(zip(test_df.target.unique(), test_df.SubjectName.unique())))

    print(f"##################### THIS MODEL IS FOR : {stream_name} #####################")
    
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")
    print(f"Number of validation questions {val_df.shape[0]}")
    
    print("###### Remove Nans and reset index ########")
    train_df = train_df.dropna()
    test_df = test_df.dropna()
    val_df = val_df.dropna()
    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)
    val_df = val_df.reset_index(drop=True)
    print("###########################################")
    
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")
    print(f"Number of validation questions {val_df.shape[0]}")
    
    print("############### SANITY CHECK ##############")
    print("Subjects among which to classify are", train_df.SubjectName.unique())
    print("Number of question for each subject are", train_df.SubjectName.value_counts())
    print("############### SANITY CHECK DONE #########")
    ################################ VECTORIZATION ################################
    ###### 1. CHARACTER LEVEL VECTORIZATION #######################################
    char_vectorizer = TextVectorization(split="character")
    char_vectorizer.adapt(list(train_df["qeds"]))
    char_vocab = char_vectorizer.get_vocabulary()
    print(char_vectorizer.get_vocabulary())
    ################################################################################
    
    ###### 2. WORD LEVEL VECTORIZATION #############################################
    max_ques_length = max_question_length(train_df)
    text_vectorizer = TextVectorization(output_mode="int", output_sequence_length=max_ques_length)
    text_vectorizer.adapt(list(train_df["processed_question"]))
    max_tokens = len(text_vectorizer.get_vocabulary())
    
    train_ds = tfds_2_inputs(train_df)
    test_ds = tfds_2_inputs(test_df)
    val_ds = tfds_2_inputs(val_df)
    print("train_ds, test_ds, val_ds are done ready for training!!")
    ################################################################################
    
    callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', 
                                                patience=3, 
                                                restore_best_weights=True,  
                                                mode="min")
    
    def model(max_tokens=max_tokens, lstm_unit=lstm_unit, output_dim=output_dim, n_classes=n_classes):

        # First Input
        input_p = tf.keras.layers.Input(shape=(1,), dtype=tf.string, name = "Input_question")
        vectors = text_vectorizer(input_p)
        embedded = tf.keras.layers.Embedding(input_dim=max_tokens, output_dim=output_dim, mask_zero=True)(vectors)
        x_1 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_unit, return_sequences = True), name="Bidirection_Layer_ques")(embedded)
        x_1 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_unit))(x_1)

        # Second Input
        input_q = tf.keras.layers.Input(shape=(1,), dtype=tf.string, name = "Input_Layer")
        char_vectors = char_vectorizer(input_q)
        char_embed = tf.keras.layers.Embedding(input_dim=len(char_vocab), output_dim = 32, mask_zero=True)(char_vectors)
        x_2 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_unit, return_sequences = True), name="Bidirection_Layer_qed")(char_embed)
        x_2 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_unit))(x_2)

        concatenated = tf.keras.layers.Concatenate()([x_1, x_2])

        # outputs
        outputs = tf.keras.layers.Dense(n_classes, activation="softmax")(concatenated)
        model = tf.keras.Model(inputs = [input_p, input_q], 
                                  outputs = outputs)
        model.compile(optimizer="adam",
                        loss="sparse_categorical_crossentropy",
                        metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

        return model
    
    model = model()
    print(model.summary())
    history = model.fit(train_ds, validation_data = val_ds, epochs=epochs, callbacks=[callback], verbose = 2)
    score = model.evaluate(test_ds, verbose = 2)
    print('Test loss    :', score[0])
    print('Test accuracy:', score[1])
    
    model.save(os.path.join(model_dir, 'model/1'))


