"""Need to run the evaluation on all the models against the experts opinion, 

- Engineering
  - Engineering-Chemistry : Engineering-Chemistry.xlxs
  - Engineering-Physics : Engineering-Physics.xlxs
  - Engineering-Mathematics : Engineering-Mathematics.xlxs

- Medical 
  - Medical-Chemistry : Medical-Chemistry.xlxs
  - Medical-Physics : Medical-Physics.xlxs
  - Medical-Botany : Medical-Botany.xlxs
  - Medical-Zoology : Medical-Zoology.xlxs

- Foundation
  - Foundation-Mathematics : Foundation-Mathematics.csv
  - Foundation-Physics : Foundation-Physics.csv
  - Foundation-Chemistry : Foundation-Chemistry.csv
  - Foundation-Biology : Foundation-Biology.csv
"""

import functools
from datetime import datetime
import subprocess
import sys

subprocess.check_call([sys.executable, "-m", "pip", "install", "tensorflow==2.8"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "matplotlib"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "seaborn"])
import pickle
import seaborn as sns
import tensorflow as tf
import pandas as pd
import os
import re
import argparse
import json
import os
import numpy as np
import csv
from pathlib import Path
import tarfile
import itertools
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from tensorflow import keras
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.utils import resample


encoded_dict_stream_dict = {"Foundation": {5: 'Physics', 2: 'Mathematics', 1: 'Chemistry', 0: 'Biology', 3: 'Mental Ability', 4: 'Others'},
                            "Engineering": {0: 'Chemistry', 1: 'Mathematics', 2: 'Physics'},
                            "Medical": {3: 'Zoology', 1: 'Chemistry', 2: 'Physics', 0: 'Botany'}}

def list_arg(raw_value):
    """argparse type for a list of strings"""
    return str(raw_value).split(",")

def confusion_matrix_plot(y_test, y_pred, sub_names):
        """Plots the confusion matrix

        Usage:
            y_test = test_df.label.values
            confusion_matrix_plot(y_test, y_pred, encoded_dict, "unigram-confusion-matrix")

        Args:
            y_test : testing data
            y_pred : prediction on test_ds
            encoded_dict : class encoding dict
            tag : name of saved figure

        Returns:
            plots a confusion matrix
        """
        CM = confusion_matrix(y_test, y_pred, normalize='true')
        sns.set(rc={'figure.figsize': (20, 20)})
        ax = sns.heatmap(CM, annot=True)
        ax.xaxis.set_ticklabels(sub_names, rotation=90)
        ax.yaxis.set_ticklabels(sub_names, rotation=0)

        plt.xlabel('Predicted')
        plt.ylabel('Truth')
        plt.show()
        # Model Output
        metrics_path = os.path.join(args.output_data, "metrics/")
        os.makedirs(metrics_path, exist_ok=True)
        plt.savefig("{}/confusion_matrix.png".format(metrics_path))


def parse_args():
    # Unlike SageMaker training jobs (which have `SM_HOSTS` and `SM_CURRENT_HOST` env vars), processing jobs to need to parse the resource config file directly
    resconfig = {}
    try:
        with open("/opt/ml/config/resourceconfig.json", "r") as cfgfile:
            resconfig = json.load(cfgfile)
    except FileNotFoundError:
        print("/opt/ml/config/resourceconfig.json not found.  current_host is unknown.")
        pass  # Ignore

    # Local testing with CLI args
    parser = argparse.ArgumentParser(description="Process")

    parser.add_argument(
        "--hosts",
        type=list_arg,
        default=resconfig.get("hosts", ["unknown"]),
        help="Comma-separated list of host names running the job",
    )
    parser.add_argument(
        "--current-host",
        type=str,
        default=resconfig.get("current_host", "unknown"),
        help="Name of this host running the job",
    )
    parser.add_argument(
        "--input-data",
        type=str,
        default="/opt/ml/processing/input/data",
    )
    parser.add_argument(
        "--input-model",
        type=str,
        default="/opt/ml/processing/input/model",
    )
    parser.add_argument(
        "--input-pickle",
        type=str,
        default="/opt/ml/processing/pickle",
    )
    parser.add_argument(
        "--output-data",
        type=str,
        default="/opt/ml/processing/output",
    )
    parser.add_argument(
        "--stream",
        type=str,
        default="streamname",
    )
    return parser.parse_args()


def process(args):
    stream = args.stream
    print("Current host: {}".format(args.current_host))
    print("input_data: {}".format(args.input_data))
    print("input_model: {}".format(args.input_model))
    print("input_picklefile: {}".format(args.input_pickle))
    print("EVALUATION IS FOR: ", args.stream)
    print("Listing contents of input model dir: {}".format(args.input_model))
    input_files = os.listdir(args.input_model)
    for file in input_files:
        print(file)
    model_tar_path = "{}/model.tar.gz".format(args.input_model)
    model_tar = tarfile.open(model_tar_path)
    model_tar.extractall(args.input_model)
    model_tar.close()
    model = tf.keras.models.load_model("{}/model/1".format(args.input_model))
    print(model)
    
    def predict(model, batch):
        inputs_p = tf.keras.Input(shape=(1,), dtype="string")
        inputs_q = tf.keras.Input(shape=(1,), dtype="string")
        outputs = model([inputs_p, inputs_q])
        inference_model = tf.keras.Model([inputs_p, inputs_q], outputs = outputs)

        preds = inference_model([batch["processed_question"].values, batch["questionExternalId"].values])
        preds = np.argmax(preds, axis=-1)
        return preds

    # Read the data
    print("Listing contents of input data dir: {}".format(args.input_data))
    data_path = os.path.join("/opt/ml/processing/input/data", f"{stream}.csv")
    pickle_path = os.path.join("/opt/ml/processing/input/pickle", "encoded_dict.pkl")
    file = open(pickle_path,'rb')
    encoded_dict = pickle.load(file)
    print(encoded_dict)
    print("Reading input data from {}".format(data_path))
    df = pd.read_csv(data_path, error_bad_lines=False)
    df = df.dropna()
    df = df.reset_index(drop=True)
    df = df[df.processed_question!="This Ques cannot be processed"]
    print(df.shape)
    preds = predict(model, df)
    final_preds = []
    for pred in preds:
        final_preds.append(encoded_dict[pred])
    df[model_name] = final_preds
    
    # Saving features 
    print("Saving final result to {}".format(output_path))
    df.to_csv(output_path, index=False)
    


if __name__ == "__main__":
    args = parse_args()
    print("Loaded arguments:")
    print(args)

    print("Environment variables:")
    print(os.environ)
    process(args)
