"""Run Medical predictions on the final input asked

Algorithm:
- predict_subject
- predict_chapter

##### List of Helper functions ########:

prepare_encoded_dicts():
    Creates a complete encoded dict
    
load_pickle(ed_path):
    Loads the pickle file
    
load_model(model_path):
    Loads the model given the model path
    
predict(model, batch, is_chapter=False):
    - predicts subject given 
      - model
      - batch 
      - is_chapter=False
    - For chapter prediction
      - model
      - batch 
      - is_chapter=True

predict_chapter(subject_name, model_path, encoded_dict_subject):
    uses predict(model, batch, is_chapter=True) to finally predict chapters and write out the results as csv
    
"""

import functools
from datetime import datetime
import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", "tensorflow==2.8"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "matplotlib"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "seaborn"])

import seaborn as sns
import pickle
import tensorflow as tf
import pandas as pd
import os
import re
import argparse
import json
import os
import numpy as np
import csv
from pathlib import Path
import tarfile
import itertools
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from tensorflow import keras
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.utils import resample


encoded_dict = {3: 'Zoology', 1: 'Chemistry', 2: 'Physics', 0: 'Botany'}

def list_arg(raw_value):
    """argparse type for a list of strings"""
    return str(raw_value).split(",")


def parse_args():
    # Unlike SageMaker training jobs (which have `SM_HOSTS` and `SM_CURRENT_HOST` env vars), processing jobs to need to parse the resource config file directly
    resconfig = {}
    try:
        with open("/opt/ml/config/resourceconfig.json", "r") as cfgfile:
            resconfig = json.load(cfgfile)
    except FileNotFoundError:
        print("/opt/ml/config/resourceconfig.json not found.  current_host is unknown.")
        pass  # Ignore

    # Local testing with CLI args
    parser = argparse.ArgumentParser(description="Process")

    parser.add_argument(
        "--hosts",
        type=list_arg,
        default=resconfig.get("hosts", ["unknown"]),
        help="Comma-separated list of host names running the job",
    )
    parser.add_argument(
        "--current-host",
        type=str,
        default=resconfig.get("current_host", "unknown"),
        help="Name of this host running the job",
    )
    parser.add_argument(
        "--input-data",
        type=str,
        default="/opt/ml/processing/input/data",
    )
    parser.add_argument(
        "--subject",
        type=str,
        default="/opt/ml/processing/model/subject",
    )
    parser.add_argument(
        "--botany",
        type=str,
        default="/opt/ml/processing/Botany/model",
    )
    parser.add_argument(
        "--chemistry",
        type=str,
        default="/opt/ml/processing/Chemistry/model",
    )
    parser.add_argument(
        "--physics",
        type=str,
        default="/opt/ml/processing/Physics/model",
    )
    parser.add_argument(
        "--zoology",
        type=str,
        default="/opt/ml/processing/Zoology/model",
    )
    parser.add_argument(
        "--encodings",
        type=str,
        default="/opt/ml/processing/encoding",
    )
    return parser.parse_args()


def process(args):
    #stream = args.stream
    print("Input data: {}".format(args.input_data))
    print("Subject Model: {}".format(args.subject))
    print("Botany Model: {}".format(args.botany))
    print("Chemistry Model: {}".format(args.chemistry))
    print("Physics Model: {}".format(args.physics))
    print("Zoology Model: {}".format(args.zoology))
    print("Encoding: {}".format(args.encodings))
    
    # Input data path
    input_data_path = args.input_data
    
    # Model Path
    subject_model_path = args.subject
    botany_model_path = args.botany
    chemistry_model_path = args.chemistry
    physics_model_path = args.physics
    zoology_model_path = args.zoology
    
    ###################### HELPER FUNCTIONS ##############################
    def load_pickle(ed_path):
        file = open(ed_path,'rb')
        encoded_dict = pickle.load(file)
        file.close()
        return encoded_dict
    
    def prepare_encoded_dicts():
        # Perform subject predictions
        ed_path_complete = os.path.join(args.encodings, "encoded_dict_complete.pkl")
        encoded_dict_complete = load_pickle(ed_path_complete)
        return encoded_dict_complete
    
    
    def load_model(model_path):
        model_tar_path = "{}/model.tar.gz".format(model_path)
        model_tar = tarfile.open(model_tar_path)
        model_tar.extractall(model_path)
        model_tar.close()
        print("Loading model..")
        model = tf.keras.models.load_model("{}/model/1".format(model_path))
        print("Model loaded!!")
        return model

    def predict(model, batch, is_chapter=False):
        inputs_p = tf.keras.Input(shape=(1,), dtype="string")
        inputs_q = tf.keras.Input(shape=(1,), dtype="string")
        outputs = model([inputs_p, inputs_q])
        print("Starting prediction...")
        inference_model = tf.keras.Model([inputs_p, inputs_q], outputs = outputs)
        if is_chapter:
            preds = inference_model([batch["processed_question"].values, batch["questionExternalId"].values])
            preds = np.argmax(preds, axis=-1)
            return preds
        else:
            preds = inference_model([batch["processed_question"].values, batch["qeds"].values])
            preds = np.argmax(preds, axis=-1)
            return preds
        
    def predict_chapter(subject_name, model_path, encoded_dict_subject):
        # Group data based on subject
        df_grped = df_medical[df_medical["subject"]==subject_name]
        
        # Load Chapter Model
        subject_model = load_model(model_path)
        
        # Start predictions
        print("Starting Predictions..")
        chapter_preds = predict(model=subject_model, batch=df_grped, is_chapter=True) # Do chapter prediction
        print("Predicting from the following chapters...")
        
        # Use encoded_dict_subject to transform predictions
        print(encoded_dict_subject)
        final_chapter_preds = [encoded_dict_subject[pred] for pred in chapter_preds]
        df_grped["chapter"] = final_chapter_preds
        preds_output_path = os.path.join(f"/opt/ml/processing/{subject_name}", "predictions.csv")
        
        # Saving features 
        print("Saving predictions to {}".format(preds_output_path))
        df_grped.to_csv(preds_output_path, index=False)
        print(f"{subject_name} Done!!")
        
    ##############################################################################

    # Perform subject predictions
    input_data_path = os.path.join("/opt/ml/processing/input/data", "final_output.csv")
    print("Reading input data from {}".format(input_data_path))
    df = pd.read_csv(input_data_path)
    print("Shape of before the drop", df.shape)
    df = df.dropna()
    df = df.reset_index(drop=True)
    df = df[df.processed_question!="This Ques cannot be processed"]
    print("Shape of after the drop", df.shape)
    df_medical = df[df.Stream == "Medical"]
    
    # Subject predictions
    print("##################### STARTING SUBJECT PREDICTION ################")
    subject_model = load_model(subject_model_path)
    subject_preds = predict(model=subject_model, batch=df_medical)
    final_subject_preds = []
    for pred in subject_preds:
        final_subject_preds.append(encoded_dict[pred])
    df_medical["subject"] = final_subject_preds 
    print("##################### SUBJECT PREDICTION DONE!! ##################")
    
    # Sanity prints
    print(df_medical["subject"].value_counts())
    
    # Chapter prediction calls
    encoded_dict_complete = prepare_encoded_dicts()
    
    # Encoded dict and Model path dictionary 
    model_encoder_dict = {"Botany": {"model": botany_model_path, 
                                     "encoded_dict": encoded_dict_complete["Medical-Botany"]},
                          "Chemistry": {"model": chemistry_model_path, 
                                       "encoded_dict": encoded_dict_complete["Medical-Chemistry"]},
                          "Physics": {"model": physics_model_path, 
                                     "encoded_dict": encoded_dict_complete["Medical-Physics"]},
                          "Zoology": {"model": zoology_model_path, 
                                     "encoded_dict": encoded_dict_complete["Medical-Zoology"]}}

    for subject in ["Botany", "Chemistry", "Physics", "Zoology"]:
        print(f"################### STARTING {subject} #####################")
        model_path = model_encoder_dict[subject]["model"]
        encoded_dict_subject = model_encoder_dict[subject]["encoded_dict"]
        predict_chapter(subject, model_path, encoded_dict_subject)
        print(f"################### {subject} Done #########################")
        print()
    

if __name__ == "__main__":
    args = parse_args()
    print("Loaded arguments:")
    print(args)

    print("Environment variables:")
    print(os.environ)

    process(args)
