"""This script is responsible for getting the final dataset ready for batch transform 
steps: 

1. Take 2 inputs
   - complete dataset (data_for_lstm.csv)
   - prepared dataset (input-1.csv)
   
2. Merge 2 datasets
3. Process questions
4. Prepare final dataset
5. Output dataset for batch transform
   
"""
import argparse
import os
import warnings
import pandas as pd
import numpy as np

        
if __name__ == "__main__":
    
    # Reading the data
    print("Preparing final dataset")
    complete_data_path = os.path.join("/opt/ml/processing/input/untagged", "untagged.csv")
    input_data_path = os.path.join("/opt/ml/processing/input/data", "final_input.csv")
    print("Reading untagged data from {}".format(complete_data_path))
    print("Reading prepped data from {}".format(input_data_path))
    df_whole = pd.read_csv(complete_data_path)
    df_prepped = pd.read_csv(input_data_path)
    
    # merge the dataset
    col_list = ["question", "processed_question", "questionExternalId", "nodeShortCode"]
    df_prepped = pd.merge(df_whole[col_list], df_prepped, on="questionExternalId", how = "inner")
    print("################## SANITY CHECKS #################")
    print("Shape of final dataset", df_prepped.shape)
    print(df_prepped["Stream"].value_counts())
    print("Node short code is", df_prepped.nodeShortCode.unique())
    df_prepped = df_prepped[df_prepped.nodeShortCode == "TE12MTH19_13_01"]
    df_missed = df_prepped[df_prepped.processed_question == "This Ques cannot be processed"]
    print("Number of questions that will be missed", df_missed.shape[0])
    df_prepped = df_prepped[df_prepped.processed_question != "This Ques cannot be processed"]
    df_prepped = df_prepped.dropna()
    df_prepped = df_prepped.reset_index(drop=True)
    print("Number of questions used in batch transform", df_prepped.shape[0])
    print("###################################################")
    
    # Setting up paths
    # missed_question_output_path = os.path.join("/opt/ml/processing/missed", "missed.csv")
    final_output_path = os.path.join("/opt/ml/processing/final", "final_output.csv")
    # Saving features 
    print("Saving prepped data to {}".format(final_output_path))
    df_prepped.to_csv(final_output_path, index=False)
    print("Done!")
    