"""This is responsible for preprocessing questions and qeds if required, after preprocessing split the data in training testing
and validation data
"""
import argparse
import os
import warnings
import pandas as pd
import numpy as np
import json  
import os
import json
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def chapter_code(code):
    return code.split("_")[0]


def filter_by_freq(df: pd.DataFrame, column: str, min_freq: int) -> pd.DataFrame:
    # Frequencies of each value in the column.
    freq = df[column].value_counts()
    # Select frequent values. Value is in the index.
    frequent_values = freq[freq >= min_freq].index
    # Return only rows with value frequency above threshold.
    return df[df[column].isin(frequent_values)]

def prepare_dataset(subject_df, node_df):
    chapters_to_consider = list(node_df["Chapter Code"])
    subject_df = subject_df[subject_df['ChapterCode'].isin(chapters_to_consider)]
    subject_df = subject_df.dropna()
    subject_df = subject_df.reset_index(drop=True)
    subject_df = filter_by_freq(subject_df, column="ChapterCode", min_freq= 10)
    return subject_df

import pickle
def pickle_dump(filename, py_obj):
    filehandler = open(filename, "wb")
    pickle.dump(py_obj, filehandler)
    filehandler.close()
    
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser()
    parser.add_argument("--train-test-split-ratio", type=float, default=0.1)
    parser.add_argument("--stream-sub", type=str, default = "Foundation-Mental-Ability")
    parser.add_argument("--output-data", type=str, default="/opt/ml/processing/output")
    args, _ = parser.parse_known_args()
    print("Received arguments {}".format(args))
    
    # Get arguments
    str_sub_name = args.stream_sub
    str_sub_tuple = tuple(str_sub_name.split("-"))
    test_size = args.train_test_split_ratio
    print("Stream and subject combination used", str_sub_tuple)
    print("Test Size: ", test_size)
    print("Stream and subject name", str_sub_name)
    
    # Download data
    input_data_path = os.path.join("/opt/ml/processing/data", "tagged.csv")
    input_codes_and_keywords = os.path.join("/opt/ml/processing/chapterCodes", f"{str_sub_name}.csv")
    
    # Sanity Checks
    print("Tagged data read from {}".format(input_data_path))
    print(f"Chapter code for {str_sub_name} from {input_codes_and_keywords}")
    
    # Get the tagged data
    df = pd.read_csv(input_data_path)
    df = pd.DataFrame(df)
    df = df[df.nodeShortCode != "TE12MTH19_13_01"]
    
    # Add chapter code
    df["ChapterCode"] = df["nodeShortCode"].map(chapter_code)
    
    # Get the group
    grouped_df = df.groupby(["stream", "SubjectName"])
    subject_df = grouped_df.get_group(str_sub_tuple)
    
    # Read stream_subject.csv 
    node_df = pd.read_csv(input_codes_and_keywords)
    node_df = node_df[["Chapter Name", "Chapter Code"]]
    
    # Filter df
    subject_df = prepare_dataset(subject_df, node_df)
    
    # Sanity prints before sending the data
    print(f"################### SANITY CHECK FOR {str_sub_name} ########################")
    print("Shape of the data", subject_df.shape)
    print("Number of questions", subject_df.shape[0])
    print("Output chapter code", subject_df["ChapterCode"].unique())
    print("Number of classes to be predicted: ", len(subject_df["ChapterCode"].unique()))
    print("Frequency distribution", subject_df.ChapterCode.value_counts())
    print("#################### SANITY CHECK DONE!!######################################")
    
    print("#################### READY FOR TRAIN-TEST SPLIT ##############################")
    print("Number of questions ", subject_df.shape[0])
    
    # Get rid of "This Ques cannot be processed" in processed_question column
    subject_df = subject_df[subject_df.processed_question!="This Ques cannot be processed"]
    subject_df = subject_df
    print(subject_df.ChapterCode.value_counts())
    
    # Add label encoder
    encoder = LabelEncoder()
    encoded = encoder.fit_transform(subject_df["ChapterCode"])
    subject_df["target"] = encoded
    encoded_dict = dict(zip(list(encoded), list(encoder.inverse_transform(encoded))))  # to be returned
    print(encoded_dict)
    inverse_encoded_dict = {v: k for k, v in encoded_dict.items()}
    
    # Split the data into training and testing
    print("Splitting data into train and test sets with ratio {}".format(test_size))
    train_df, test_df = train_test_split(subject_df, test_size=float(test_size), stratify=subject_df["target"], random_state=42, shuffle=True)
    
    # Printing necessary information
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")
    print("###### Remove Nans and reset index ########")
    train_df = train_df.dropna()
    test_df = test_df.dropna()
    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)
    print(f"Number of training questions {train_df.shape[0]}")
    print(f"Number of testing questions {test_df.shape[0]}")

    train_features_output_path = os.path.join("/opt/ml/processing/train", "train.csv")
    test_features_output_path = os.path.join("/opt/ml/processing/test", "test.csv")
    
    # Saving features 
    print("Saving training features to {}".format(train_features_output_path))
    train_df.to_csv(train_features_output_path, index=False)

    print("Saving test features to {}".format(test_features_output_path))
    test_df.to_csv(test_features_output_path, index=False)
    
    encoded_dict_path = os.path.join(args.output_data, "encoded_dict/")
    print("Starting Pickling")
    encoded_dict_fp = "{}/encoded_dict.pkl".format(encoded_dict_path)
    pickle_dump(filename = encoded_dict_fp, py_obj = encoded_dict)
    print("Pickling Done!!")
    print("ALL DONE !!")