"""This is responsible for processing questions and qeds for all the questions present in lcms currently.
This job will do the following
1. add processed_questions
2. add processed_qeds
3. Output of this job will be two csv files namely,
   - tagged_data.csv
   - untagged_data.csv
"""
import argparse
import os
import warnings
import pandas as pd
import numpy as np
import json  
import re
import os
from bs4 import BeautifulSoup
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def clean_text(ques: str, quesType: str):
    if quesType in ["SCMCQ", "MCMCQ"]:
        try:
            obj = json.loads(ques)
            question_html = obj["text"]
            choices_html = obj["choices"]
            questionAndChoices = BeautifulSoup(question_html, 'lxml').get_text()
            for i in range(len(choices_html)):
                questionAndChoices += " " + BeautifulSoup(choices_html[i]["text"], 'lxml').get_text()
            sentence = questionAndChoices
            sentence = re.sub(r"http\S+", " ", sentence)
            sentence = re.sub("\S*\d\S*", " ", sentence).strip()
            sentence = re.sub('[^A-Za-z\d\s]', ' ', sentence)
            sentence = re.sub('(\\b[A-Za-z] \\b|\\b [A-Za-z]\\b)', " ", sentence)
            sentence = ' '.join(e.lower() for e in sentence.split() if len(e) > 1)
            return sentence
        except Exception as e:
            return "This Ques cannot be processed"
    else:
        try:
            obj = json.loads(ques)
            question_html = obj["text"]
            sentence = BeautifulSoup(question_html, 'lxml').get_text()
            sentence = re.sub(r"http\S+", " ", sentence)
            sentence = re.sub("\S*\d\S*", " ", sentence).strip()
            sentence = re.sub('[^A-Za-z\d\s]', ' ', sentence)
            sentence = re.sub('(\\b[A-Za-z] \\b|\\b [A-Za-z]\\b)', " ", sentence)
            sentence = ' '.join(e.lower() for e in sentence.split() if len(e) > 1)
            return sentence
        except Exception as e:
            return "This Ques cannot be processed"

def clean_qed(qed):
    return re.sub(r'[^a-zA-Z]', '', qed)


def process(df):
    df = df.reset_index(drop=True)
    processed_questions, qeds = [], []
    for idx in df.index:
        question = df.iloc[idx]["question"]
        ques_type = df.iloc[idx]["questionType"]
        qed = df.iloc[idx]["questionExternalId"]
        qeds.append(clean_qed(qed))
        processed_questions.append(clean_text(question, ques_type))
    df["processed_question"] = processed_questions
    df["qeds"] = qeds
    return df
    
    

if __name__ == "__main__":
    
    """Reading data from /opt/ml/preprocessing/input/completeData.csv
    Do Following preprocessing steps : 
    - Get complete data call it df
    - Run process job and get tagged and untagged data
    """
    input_data_path = os.path.join("/opt/ml/processing/input", "data_for_lstm.csv")
    print("Reading input data from {}".format(input_data_path))
    df = pd.read_csv(input_data_path)
    df = pd.DataFrame(df)
    
    # Add rename subjects
    subjects_to_consider = ["Mathematics", 
                            "Physics", 
                            "Chemistry", 
                            "Biology", 
                            "Mental Ability",
                            "Botany",
                            "Zoology"]
    
    subjects_not_considered = set(df["SubjectName"].unique())-set(subjects_to_consider)
    df['SubjectName'].mask(df['SubjectName'].isin(subjects_not_considered), "Others", inplace=True)
    
    # process dataset
    df = process(df)
    print(df.SubjectName.value_counts())
    
    df_tagged = df[df.nodeShortCode != "TE12MTH19_13_01"]
    df_untagged = df[df.nodeShortCode == "TE12MTH19_13_01"]
    
    # Printing necessary information
    print(f"Number of questions in tagged dataset before {df_tagged.shape[0]}")
    print(f"Number of questions in untagged dataset {df_untagged.shape[0]}")
    print("###### Reset index ########")
    df_tagged = df_tagged.reset_index(drop=True)
    df_untagged = df_untagged.reset_index(drop=True)
    
    # Number of duplicate questions
    print("Number of duplicate questions in tagged data", df_tagged.shape[0]-df_tagged.dropna().shape[0])
    print("Number of duplicate questions in un-tagged data", df_untagged.shape[0]-df_untagged.dropna().shape[0])

    tagged_output_path = os.path.join("/opt/ml/processing/tagged", "tagged.csv")
    untagged_output_path = os.path.join("/opt/ml/processing/untagged", "untagged.csv")
    
    # Saving features 
    print("Saving tagged data to {}".format(tagged_output_path))
    df_tagged.to_csv(tagged_output_path, index=False)

    print("Saving untagged data to {}".format(untagged_output_path))
    df_untagged.to_csv(untagged_output_path, index=False)

